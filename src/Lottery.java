import java.util.ArrayList;

public class Lottery {

    private int startNumber, endNumber;
     Lottery(int startNumber, int endNumber) {
         this.startNumber = startNumber;
         this.endNumber = endNumber;
     }
     void calculate() {
         ArrayList<Integer>tickets = generateTickets();
         int countHappyTicketsEasy = easyMethod(tickets);
         int countHappyTicketsHard = hardMethod(tickets);

         System.out.println("Количество счастливых билетов в простом методе: " + countHappyTicketsEasy);
         System.out.println("Количество счастливых билетов в сложном методе: " + countHappyTicketsHard);

         if (countHappyTicketsEasy > countHappyTicketsHard) {
             System.out.println("Простой метод собрал больше счастливых билетов!");
         } else if (countHappyTicketsEasy < countHappyTicketsHard){
             System.out.println("Сложный метод собрал больше счастливых билетов!");
         } else {
             System.out.println("Методы собрали поровну счастливых билетов!");
         }
     }
     private ArrayList<Integer>generateTickets() {
         ArrayList<Integer>listTickets = new ArrayList<Integer>();
          for(int i = startNumber; i <=endNumber; i++) {
              listTickets.add(i);
          }
          return listTickets;
     }
     private int easyMethod(ArrayList<Integer>tickets) {
         int countTicket = 0;

         for(Integer ticket: tickets) {
             if(ticket >=100000 && ticket <=999999) {
                 String ticketString = String.valueOf(ticket);
                 String[] numbers = ticketString.split("");

                 int firstHalfSum = 0;
                 int secondHalfSum = 0;

                 for(int j = numbers.length/2; j < numbers.length; j++) {
                     secondHalfSum += Integer.parseInt(numbers[j]);
                 }
                 if(firstHalfSum == secondHalfSum) {
                     countTicket++;
                 }
             }
         }
         return countTicket;
     }
     private int hardMethod(ArrayList<Integer>tickets) {
          int countTicket = 0;

          for(Integer ticket: tickets) {
              String ticketString = String.valueOf(ticket);
              String[] numbers = ticketString.split("");

              int firstHalfSum = 0;
              int secondHalfSum = 0;

              for(int j = 1; j < numbers.length; j += 2) {
                  secondHalfSum += Integer.parseInt(numbers[j]);
              }
              if(firstHalfSum == secondHalfSum) {
                  countTicket++;
              }
          }
          return countTicket;
     }
}
